;(function($){

	"use strict";

	var Core = {

		DOMReady: function(){

			var self = this;
			
			self.backToTopBtn({
			    transitionIn: 'bounceInRight',
			    transitionOut: 'bounceOutRight'
			});

			self.searchOverflow();
			self.characteristic();
			self.discontCal.init();
			self.filePreview();
			self.events();

		},

		windowLoad: function(){

			var self = this;
			
		},

		/**
		**	Back to top
		**/

		backToTopBtn: function(config){

			config = $.extend({
				offset: 350,
				transitionIn: 'bounceInRight',
				transitionOut: 'bounceOutRight'
			}, config);

			var btn = $('<button></button>', {
				class: 'back_to_top animated hide',
				html: '<i class="material-icons">expand_less</i>'
			}).appendTo($('body')),

			$wd = $(window),
			$html = $('html'),
			$body = $('body');

			$wd.on('scroll.back_to_top', function(){

				if($wd.scrollTop() > config.offset){

					btn.removeClass('hide '+config.transitionOut).addClass(config.transitionIn);

				}
				else{

					btn.removeClass(config.transitionIn).addClass(config.transitionOut);

				}

			});

			btn.on('click', function(){

				$html.add($body).animate({

					scrollTop: 0

				});

			});

	   	},


		searchOverflow: function(){

			$('#search').focus(function(){
				$(this).closest('.search_wrap').addClass('focus');
				$('.search_wrap_overflow').addClass('active');
			})

			$('#search').blur(function(){
				$(this).closest('.search_wrap').removeClass('focus');
				$('.search_wrap_overflow').removeClass('active');
			})
		},

		characteristic: function(){

			function addCharacteristic(charName, charVal, id, labelClass){

				var $template = '<div class="row characteristic_item" data-id="'+id+'">\
                                    <div class="input-field col l5 s9">\
                                        <input id="charid_'+id+'" type="text" value="'+charVal+'">\
                                        <label for="charid_'+id+'" class="'+labelClass+'">'+charName+'</label>\
                                    </div>\
                                    <div class="col l1 s3">\
                                    	<div class="characteristic_btn_wrapp">\
                                             <a href="javascript:;"  class="characteristic_remove_btn"><i class="material-icons">close</i></a>\
                                        </div>\
                                    </div>\
                                </div>';

				$('.characteristic_wrap').append($template);

			}

			$('.characteristic_wrap').on('click', '.characteristic_show_btn', function(){
				var $item = $(this).closest('.characteristic_item');

				if($item.hasClass('char_hidde')){
					$item.removeClass('char_hidde');
					$item.find('input').prop('disabled', false);
				}else{
					$item.addClass('char_hidde');
					$item.find('input').prop('disabled', true);
				}
			});

			$('.characteristic_wrap').on('click', '.characteristic_remove_btn', function(){
				
				var $item = $(this).closest('.characteristic_item'),
					$id = $item.attr('data-id');

				$('.characteristic_list').find('.collection-item[data-id="'+$id+'"]').removeClass('active')
				$item.slideUp(500, function(){
					$item.remove();
				})				
			});

			$('body').on('click', '.characteristic_add_btn', function(){
				
				var $item = $(this).closest('.row'),
					$charNameInp = $item.find("#characteristic_name"),
					$charValInp = $item.find("#characteristic_val"),
					$charName = $item.find("#characteristic_name").val(),
					$charVal = $item.find("#characteristic_val").val();

				if($charName == ""){
					$charNameInp.focus();
					$charNameInp.blur();
				}
				else if ($charVal == "") {
					$charValInp.focus();
					$charValInp.blur();
				}
				else{
					addCharacteristic($charName, $charVal, $charName, "active");
					$charNameInp.val('');
					$charValInp.val('');
					$('.add_new_char_box').slideUp(500);
				}
							
			});
			
			$('body').on('click', '.characteristic_cancel_btn', function(){
				
				var $item = $(this).closest('.row'),
					$charNameInp = $item.find("#characteristic_name"),
					$charValInp = $item.find("#characteristic_val");
					
				$charNameInp.val('');
				$charValInp.val('');
				$('.add_new_char_box').slideUp(500);
							
			});

			$('body').on('click', '.characteristic_open_box_btn', function(){
				
				$('.add_new_char_box').slideDown(500);
							
			});

			$('.characteristic_list>li').on('click', function(){
				var $charName = $(this).find('.characteristic_list_text').text(),
					$id = $(this).attr('data-id'),
					$val = $(this).attr('data-val');
				
				if($(this).hasClass('active')){
					$('.characteristic_wrap').find('.characteristic_item[data-id="'+$id+'"]').slideUp(500, function(){
						$('.characteristic_wrap').find('.characteristic_item[data-id="'+$id+'"]').remove();
					})
					$(this).removeClass('active');
				}else{
					addCharacteristic($charName, $val, $id, "active");
					$(this).addClass('active');
				}
			})

			$('body').on('click', '.characteristic_open_box_btn2', function(){
				
				var $index = $('.characteristic_wrap>.row').length + 1,
					$template = '<div class="row">\
                                            <div class="input-field col l6 s12">\
                                                <input id="charid_name_'+$index+'" type="text">\
                                                <label for="charid_name_'+$index+'">Характеристика</label>\
                                            </div>\
                                            <div class="input-field col l6 s12">\
                                                <input id="charid_val_'+$index+'" type="text">\
                                                <label for="charid_val_'+$index+'">Значение</label>\
                                            </div>\
                                        </div>';

                $('.characteristic_wrap').append($template);

			});
			
		},


		discontCal:{
			
			init: function(){
				var self = this;
				self.valute = $('#valute').val();
				self.courseEvro = $('#course_evro').val();

				self.events();
			},

			calculate: function(el){
				
				var self = this,
					$price = el.attr('data-price'),
					$discount = el.attr('data-discount'),
					$valute = el.attr('data-valute'),
					$discountValute = el.attr('data-discount-valute'),
					$sumEl = el.find('.product_sum'),
					$discountEl = el.find('.product_discount'),
					$sumElValut = $('.icon_valut'),
					$productQt = el.find('.product_qt').val(),
					$sum = 0,
					$carrentValut = '<i class="fa fa-rub" aria-hidden="true"></i>',
					$result,
					$resultDisSet,
					$resultDis;

				if($valute != self.valute){
					if($valute == 0){
						$price = $price*self.courseEvro;
					}else{
						$price = $price/self.courseEvro;
					}
				}

				if(self.valute != 0){
					$sumElValut.html('<i class="fa fa-rub" aria-hidden="true"></i>');
					$carrentValut = '<i class="fa fa-rub" aria-hidden="true"></i>';
				}
				else{
					$sumElValut.html('<i class="fa fa-eur" aria-hidden="true"></i>');
					$carrentValut = '<i class="fa fa-eur" aria-hidden="true"></i>';
				}

				if($discount && $discountValute == 2){
					$sum = $price - ($price/100*$discount);
					$resultDis = $price/100*$discount;
					$resultDisSet = $discount+'%';
				}
				else if($discount && $discountValute == 1){
					$resultDisSet = $discount+'<i class="fa fa-rub" aria-hidden="true"></i>';
					if(self.valute == 1){
						$sum = $price - $discount;
						$resultDis = $discount;
					}else{
						$sum = $price - ($discount/self.courseEvro);
						$resultDis = $discount/self.courseEvro;
						// $resultDisSet = ($discount/self.courseEvro)/($price/100);
					}
					// $discountEl.html($discount+'₽')
				}
				else if($discount && $discountValute == 0){
					$resultDisSet = $discount+'<i class="fa fa-eur" aria-hidden="true"></i>';
					if(self.valute == 0){
						$sum = $price - $discount;
						$resultDis = $discount;
					}else{
						$sum = $price - ($discount*self.courseEvro);
						$resultDis = ($discount*self.courseEvro);
					}
					// $discountEl.html($discount+'&euro;')
				}

				el.attr('data-resulsum', ($sum * $productQt));
				el.attr('data-resultdis', ($resultDis * $productQt));

				$result = Math.round($sum * $productQt);
				$result = $result+'';
				$result = $result.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1\u00A0')
				$sumEl.text($result);

				$resultDis = Math.round($resultDis * $productQt);
				$resultDis = $resultDis+'';
				$resultDis = $resultDis.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1\u00A0');
				if($discountValute == 2){
					$discountEl.html($resultDisSet+' ('+$resultDis+' '+$carrentValut+')');
				}
				else{
					$discountEl.html($resultDis+' '+$carrentValut);	
				}

				self.calculateTotal();
			},

			calculateTotal: function(){

				var self = this,
					$elSumTotal = $('.sum_total'),
					$elDiscountTotal = $('.discount_total'),
					$elDiscountTotal2 = $('.discount_total2'),
					$elVatTotal = $('.vat_total'),
					$elPrepaymentTotal = $('.prepayment_total'),
					$elPostpayTotal = $('.postpay_total'),

					$prepaymentTotalVal = $('#prepayment').val(),
					$discountTotalVal = $('#discount').val(),
					
					$totalSum = 0,
					$totalSum2 = 0,
					$totalDis = 0,
					$totalDis2 = 0,
					$totalVat = 0,
					$postpayTotal = 0,
					$totalPrepayment = 0;

				$('.product_qt').each(function(){

					var $el = $(this).closest('tr'),
						$sumItem = $el.attr('data-resulsum'),
						$discountItem = $el.attr('data-resultdis');

					$totalSum += (+$sumItem);
					$totalDis += (+$discountItem);

				});
				
				$('.prepayment_total_percent').html($prepaymentTotalVal+'%');
				$('.postpay_total_percent').html(100-$prepaymentTotalVal+'%');
				$('.discount_total2_percent').html($discountTotalVal+'%');

				$totalVat = $totalSum/1.2*0.2;
				$totalDis2 = $totalSum/100*$discountTotalVal;
				$totalSum2 = $totalSum - $totalDis2;
				$totalPrepayment = $totalSum2/100 * $prepaymentTotalVal;
				$postpayTotal = $totalSum2 - $totalPrepayment;

				$totalSum = Math.round($totalSum);
				$totalSum = $totalSum+'';
				$totalSum = $totalSum.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ')
				$elSumTotal.text($totalSum);
				
				$totalDis = Math.round($totalDis);
				$totalDis = $totalDis+'';
				$totalDis = $totalDis.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ')
				$elDiscountTotal.text($totalDis);

				$totalDis2 = Math.round($totalDis2);
				$totalDis2 = $totalDis2+'';
				$totalDis2 = $totalDis2.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ')
				$elDiscountTotal2.text($totalDis2);

				$totalVat = Math.round($totalVat);
				$totalVat = $totalVat+'';
				$totalVat = $totalVat.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ')
				$elVatTotal.text($totalVat);

				$totalPrepayment = Math.round($totalPrepayment);
				$totalPrepayment = $totalPrepayment+'';
				$totalPrepayment = $totalPrepayment.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ')
				$elPrepaymentTotal.text($totalPrepayment);

				$postpayTotal = Math.round($postpayTotal);
				$postpayTotal = $postpayTotal+'';
				$postpayTotal = $postpayTotal.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ')
				$elPostpayTotal.text($postpayTotal);

			},

			defaultValue: function(){

				var self = this;
				var $box = $('[data-euro_def]'),
					$euroDef = $box.attr('data-euro_def'),
					$discountDef = $box.attr('data-discount_def'),
					$prepaymentDef = $box.attr('data-prepayment_def'),
					$valuteDef = $box.attr('data-valute_def');

				$('#course_evro').val($euroDef);
				$('#discount').val($discountDef);
				$('#prepayment').val($prepaymentDef);
				$('#valute').val($valuteDef).formSelect();
				
				self.courseEvro = $('#course_evro').val();
				self.valute = $('#valute').val();

				$('.product_qt').each(function(){

					var $el = $(this).closest('tr');

					self.calculate($el);

				});

			},

			events: function(){
				
				var self = this;

				if($('#use_default').length){
					var $this = $('#use_default'),
						$check = $this.prop('checked');
					if($check){
						self.defaultValue();
					}
					else{
						
						$('.product_qt').each(function(){

							var $el = $(this).closest('tr');

							self.calculate($el);

						});
					}
				}

				$('.product_qt').on('change keyup input click', function(){

					var $el = $(this).closest('tr');

					self.calculate($el);

				});

				$('#prepayment').on('change keyup input click', function(){

					self.calculateTotal();

				});

				$('#discount').on('change keyup input click', function(){

					self.calculateTotal();

				});

				$('#course_evro').on('blur', function(){
					
					self.courseEvro = $(this).val();
					
					$('.product_qt').each(function(){

						var $el = $(this).closest('tr');

						self.calculate($el);

					});

				});

				$('#valute').on('change', function(){

					self.valute = $(this).val();

					$('.product_qt').each(function(){

						var $el = $(this).closest('tr');

						self.calculate($el);

					});

				});

				$('#use_default').on('change', function(){
					var $this = $(this),
						$check = $this.prop('checked');
					if($check){
						$this.closest('fieldset').find('input:not(#use_default), select, textarea').prop('disabled', true);
						$this.closest('fieldset').find('select').formSelect();
						self.defaultValue();
					}
					else{
						$this.closest('fieldset').find('input:not(#use_default), select, textarea').prop('disabled', false);
						$this.closest('fieldset').find('select').formSelect();
					}
				})

				$('body').on('click', '.del_spec_btn', function(){
					var $neme = $(this).closest('tr').find('[data-th="rate_review"]>a').text(),
						$modal = $(this).attr('href');

					$($modal).find('.name_spec').text($neme);

				})

				$('body').on('click', '.delete_item', function(){

					$(this).closest('tr').animate({
						'opacity': 0
					},500, function(){
						$(this).closest('tr').remove();
						self.calculateTotal();
					})
				})


			}
		},

		filePreview: function(){

			function readURL($files, $input) {
			  	var reader = new FileReader();
			    reader.onload = function(e) {
			    	$input.closest('.file-field').find('.file_preview_box').removeClass('hide');
			    	$input.closest('.file-field').find('.file_preview_box>a').attr('href', e.target.result);
			    	$input.closest('.file-field').find('.file_preview_box>a>img').attr('src', e.target.result).attr('alt', $files.name);
			    }
			    
			    reader.readAsDataURL($files);

			}

			function readURL2($files, $input) {
			  	var reader = new FileReader();
			    reader.onload = function(e) {
			    	var $box = $input.closest('.js_file_box'),
				  		$inputName = $box.find('input[type="radio"]').attr('name'),
				  		$galleryBox = $box.find('.popup-gallery'),
				  		$tempate = '<div class="col m3 s6">\
	                                        <label class="gallery_label">\
	                                            <a href="'+e.target.result+'" class="gallery_link"><img src="'+e.target.result+'"></a>\
	                                            <input name="'+$inputName+'" type="radio" checked/>\
	                                            <span>Выбрать</span>\
	                                        </label>\
	                                    </div>';
			    	$galleryBox.append($tempate);
			    }
			    
			    reader.readAsDataURL($files);

			}

			$(document).on('change', '.js-load_preview', function (e) {

				var $this = $(this),
					$files = e.target.files[0];
				readURL($files, $this);

			});

			$(document).on('click', '.js-load_preview_close', function (e) {

				var $this = $(this),
					$preview = $this.closest('.js-load_preview_box'),
					$input = $this.closest('.file-field').find('.js-load_preview');

				$this.closest('.file-field').find('input.file-path').val('');
				$input.val('');
				$preview.remove();

			});

			$(document).on('change', '.js_file_add', function (e) {

				var $this = $(this),
					$files = e.target.files[0];
				readURL2($files, $this);

			});

		},

		events: function(){

			// $('.product_qt').each(function(){

			// 	var $this = $(this),
			// 		$price = $this.attr('data-price'),
			// 		$parent = $this.closest('tr'),
			// 		$sum = $parent.find('.product_sum');

			// 	$sum.text($this.val()*$price);

			// });

			// $('.product_qt').on('change keyup input click', function(){

			// 	var $this = $(this),
			// 		$price = $this.attr('data-price'),
			// 		$parent = $this.closest('tr'),
			// 		$sum = $parent.find('.product_sum');

			// 	$sum.text($this.val()*$price);

			// });
			// 
			
			$(document).on('click', '.product_desc_char_label', function(){
				if($(this).hasClass('active')){
					$(this).removeClass('active');
					$(this).find('.product_desc_char_label_icon').html('expand_more');
					$(this).siblings('.product_desc_char').stop().slideUp();
				}
				else{
					$(this).addClass('active');
					$(this).find('.product_desc_char_label_icon').html('expand_less');
					$(this).siblings('.product_desc_char').stop().slideDown();	
				}
			})

			
			$(document).on('click', '.print_doc', function(){
				window.print();
			})

			$('.form_submit1').on('click', function(){
				$('#form1').submit();
				$('#form2').submit();
			})

			// $('.reset_filter_btn').on('click', function(){
			// 	$('#datepicker_from').val('');
			// 	$('#datepicker_to').val('');
			// 	$(this).closest('form').submit();
			// 	M.updateTextFields();
			// });

			if($('#own_text').length){
				var $this = $('#own_text'),
					$check = $this.prop('checked');
				if($check){
					$('.own_text_box').slideDown(500);
				}
				else{
					$('.own_text_box').slideUp(500);
				}
			}

			$('#own_text').on('change', function(){
				var $this = $(this),
					$check = $this.prop('checked');
				if($check){
					$('.own_text_box').slideDown(500);
				}
				else{
					$('.own_text_box').slideUp(500);
				}
			})

			$('body').on('click', '.add_vendor_code', function(){
				var $this = $(this),
					$type = $this.attr('data-type'),
					$typeIndex = $this.closest('ul').children('li').length -1,
					$parent = $this.closest('li'),
					$template = '<li class="collection-item">\
                                    <div class="row">\
                                        <div class="input-field col l5 s12">\
                                            <input id="'+$type+$typeIndex+'_to_contract" class="inp_contract" type="text">\
                                            <label for="'+$type+$typeIndex+'_to_contract">Модификация</label>\
                                        </div>\
                                        <div class="input-field col l5 s12">\
                                            <input id="'+$type+$typeIndex+'_vendorcode" class="inp_vendorcode" type="text">\
                                            <label for="'+$type+$typeIndex+'_vendorcode">Артикул</label>\
                                        </div>\
                                        <div class="input-field col l2 s12">\
                                            <a class="add_vendor_code2 waves-effect waves-light btn btn_radius btn2">Готово</a>\
                                        </div>\
                                    </div>\
                                </li>';

				$parent.before($template);
				
			})


			$('body').on('click', '.add_vendor_code2', function(){
				var $this = $(this),
					$parent = $this.closest('li'),
					$contractInp = $parent.find('.inp_contract'),
					$contractId = $contractInp.attr('id'),
					$vendorcodeInp = $parent.find('.inp_vendorcode'),
					$venCodeText = $vendorcodeInp.val().length ? ' ('+$vendorcodeInp.val()+')' : '',
					$template = '<li class="collection-item" data-vcode="'+$vendorcodeInp.val()+'">\
									<div>\
                                        <input type="checkbox" name="'+$contractId+'" checked>\
                                    </div>\
                                    <span class="grey-text">'+$contractInp.val()+ $venCodeText +'</span>\
                                    <a class="js_add_modif modif_added waves-effect waves-light btn btn_radius"><i class="material-icons left">check</i>Выбрано</a>\
                                </li>';

				$parent.before($template);
				$parent.slideUp(500, function(){
					$parent.remove();
				})

				modificationCalculation();
				
			});

			$('body').on('click', '.settings_edit_btn', function(){
				var $this = $(this);
				if($this.hasClass('active')){
					$this.removeClass('active');
					$this.closest('.settings_item').find('input, textarea').prop('disabled', true);
				}
				else{
					$this.addClass('active');
					$this.closest('.settings_item').find('input, textarea').prop('disabled', false);
				}
			});

			$('body').on('click', '.settings_del_btn', function(){
				var $this = $(this);
				
				$this.closest('.settings_item').slideUp(500, function(){
					$this.closest('.settings_item').remove();
				});
				
			});

			$('body').on('click', '.add_settings_item', function(){
				var $this = $(this),
					$wrapp = $this.prev('.settings_wrap'),
					$itemNeme = $wrapp.attr('data-item'),
					$itemLabel = $wrapp.attr('data-label'),
					$itemLabel2 = $wrapp.attr('data-label2'),
					$qtItem = $wrapp.find('.settings_item').length,
					$template;


				if($itemNeme == 'manager'){
					$template = '<div class="row settings_item">\
                                    <div class="input-field col l5 s9">\
                                        <input id="'+$itemNeme+'_settings_item_'+$qtItem+'" type="text">\
                                        <label for="'+$itemNeme+'_settings_item_'+$qtItem+'">'+$itemLabel+'</label>\
                                    </div>\
                                    <div class="file-field input-field col l5 s9 preview_box_wrapper">\
                                        <div class="file_preview_box hide">\
                                            <a href="" class="file_preview_link">\
                                                <img src="">\
                                            </a>\
                                        </div>\
                                        <div class="waves-effect waves-light btn btn_radius btn1 grey darken-4">\
                                            <span>'+$itemLabel2+'</span>\
                                            <input type="file" class="js-load_preview">\
                                        </div>\
                                        <div class="file-path-wrapper">\
                                            <input class="file-path validate" type="text">\
                                        </div>\
                                    </div>\
                                    <div class="col l2 s3">\
                                         <div class="settings_btn_wrapp">\
                                             <a href="javascript:;" class="settings_del_btn"><i class="material-icons">close</i></a>\
                                         </div>\
                                    </div>\
                                </div>';
				}
				else{

					$template = '<div class="row settings_item">\
                                    <div class="col l5 s9">\
                                        <div class="input-field">\
                                            <input id="'+$itemNeme+'_settings_item_'+$qtItem+'" type="text">\
                                        	<label for="'+$itemNeme+'_settings_item_'+$qtItem+'">'+$itemLabel+'</label>\
                                        </div>\
                                        <div class="file-field input-field preview_box_wrapper">\
	                                        <div class="file_preview_box hide">\
	                                            <a href="" class="file_preview_link">\
	                                                <img src="">\
	                                            </a>\
	                                        </div>\
                                            <div class="waves-effect waves-light btn btn_radius btn1 grey darken-4">\
                                                <span>Загрузить фото печати</span>\
                                                <input type="file" class="js-load_preview">\
                                            </div>\
                                            <div class="file-path-wrapper">\
                                                <input class="file-path validate" type="text">\
                                            </div>\
                                        </div>\
                                    </div>\
                                    <div class="input-field col l5 s9">\
                                        <textarea id="'+$itemNeme+'_requisites'+$qtItem+'" class="materialize-textarea"></textarea>\
                                        <label for="'+$itemNeme+'_requisites'+$qtItem+'">'+$itemLabel2+'</label>\
                                    </div>\
                                    <div class="col l2 s3">\
                                         <div class="settings_btn_wrapp">\
                                             <a href="javascript:;" class="settings_del_btn"><i class="material-icons">close</i></a>\
                                         </div>\
                                    </div>\
                                </div>';
				}
				$wrapp.append($template);
			});

			$('.specifications_table tbody tr').dblclick(function(){
				var $link = $(this).find('.link_type1');
				window.location = $link.attr('href');
			});

			$('tr[data-link]').dblclick(function(e){
				if(!$(e.target).closest('.input-field').length){
					window.location = $(this).attr('data-link');
				}
			});

			$('.collapsibles_list2').on('click', '.js_add_modif', function(){
				var $this = $(this),
					$box = $this.closest('li.collection-item'),
					$input = $box.find('input');

				if($this.hasClass('modif_added')){
					$this.removeClass('modif_added').addClass('btn1 grey darken-4').html('<i class="material-icons left">add</i>Добавить');
					$input.prop('checked', false);
				}
				else{
					$this.addClass('modif_added').removeClass('btn1 grey darken-4').html('<i class="material-icons left">check</i>Выбрано');
					$input.prop('checked', true);
				}
				modificationCalculation();
			})

			$('.js_clear_total_modif').on('click', function(){
				var $item = $('.js_add_modif.modif_added'),
					$btn = $('.js_add_total_modif');

				$item.each(function(){
					var $this = $(this),
						$box = $this.closest('li.collection-item'),
						$input = $box.find('input');
					$this.removeClass('modif_added').addClass('btn1 grey darken-4').html('<i class="material-icons left">add</i>Добавить');
					$input.prop('checked', false);
				})
				modificationCalculation();
			});

			function modificationCalculation(){

				var $item = $('.js_add_modif.modif_added'),
					$btn = $('.js_add_total_modif'),
					$qt = 0;
				if($item.length){
					$btn.addClass('active').find('span').html("("+$item.length+")")
				}
				else{
					$btn.removeClass('active').find('span').html("("+$item.length+")")
				}
				

			}

		}

	}


	$(document).ready(function(){

		Core.DOMReady();

	});

	$(window).on('load',function(){

		Core.windowLoad();

	});

})(jQuery);