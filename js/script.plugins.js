;(function($){

	"use strict";

	$(document).ready(function(){

	/* ------------------------------------------------
		Sortable START
	------------------------------------------------ */
		var fixHelper = function(e, ui) {
			ui.children().each(function() {
				$(this).width($(this).width());
			});
			return ui;
		};	
		if($('.sortable_table tbody').length){

			$('.sortable_table tbody').sortable({
				handle: '.handle',
				helper: fixHelper,
				stop: function(event, ui){
					var $elSort = $(ui.item),
						$elNext = $elSort.next(),
						$idSort = $elSort.attr('data-id'),
						$idNext = $elNext.attr('data-id');
					alert("Перетаскиваемый ID: "+ $idSort + "; Следующий ID: "+$idNext)
				}
			});

		}
	/* ------------------------------------------------
		Sortable END
	------------------------------------------------ */

	/* ------------------------------------------------
		Clipboard START
	------------------------------------------------ */

			function clipBoard(){
				
        		if($('body .copy_link').length){
        			// console.log(1)
        			new ClipboardJS('body .copy_link');
        			$('body .copy_link').on('click', function(e){
    					e.preventDefault();
    					$('.info_modal').addClass('active');
    					setTimeout(function(){
    						$('.info_modal').removeClass('active');
    					},5000)
        			})
        		}
    		}
    		clipBoard();

    		$('.info_modal_close').on('click',function(){
    			$(this).closest('.info_modal').removeClass('active');
    		})

		/* ------------------------------------------------
		Clipboard END
		------------------------------------------------ */

		/* ------------------------------------------------
				collapsible
		------------------------------------------------ */

			if($('.collapsibles_list').length){

				$('.collapsibles_list .collapsible').collapsible({
			      accordion: false
			    })
			    var instance = M.Collapsible.getInstance($('.collapsibles_list .collapsible'));
			    instance.open();
			}

			if($('.collapsibles_list2').length){

				$('.collapsibles_list2 .collapsible').collapsible()
			    
			}

        /* ------------------------------------------------
				End of collapsible
		------------------------------------------------ */


        /* ------------------------------------------------
				formSelect
		------------------------------------------------ */

			if($('select').length){

				$('select').formSelect();
			
			}

        /* ------------------------------------------------
				End of formSelect
		------------------------------------------------ */


        /* ------------------------------------------------
				magnificPopup
		------------------------------------------------ */

			if($('.popup-gallery').length){

				$('.popup-gallery').each(function(){

					$(this).magnificPopup({
						delegate: 'a.gallery_link',
						type: 'image',
						tLoading: 'Loading image #%curr%...',
						mainClass: 'mfp-img-mobile',
						gallery: {
							enabled: true,
							navigateByImgClick: true,
							preload: [0,1] // Will preload 0 - before current, and 1 after the current image
						},
						image: {
							tError: '<a href="%url%">The image #%curr%</a> could not be loaded.'
						}
					});
				})

			}

			if($('.popup-gallery2').length){

				$('.popup-gallery2').magnificPopup({
					delegate: 'a.gallery_link',
					type: 'image',
					tLoading: 'Loading image #%curr%...',
					mainClass: 'mfp-img-mobile',
					gallery: {
						enabled: true,
						navigateByImgClick: true,
						preload: [0,1] // Will preload 0 - before current, and 1 after the current image
					},
					image: {
						tError: '<a href="%url%">The image #%curr%</a> could not be loaded.'
					}
				});

			}

			if($('.file_preview_link').length){

				$('.settings_wrap').magnificPopup({
					delegate: 'a.file_preview_link',
					type: 'image',
					mainClass: 'mfp-img-mobile',
					image: {
						tError: '<a href="%url%">The image #%curr%</a> could not be loaded.'
					}
				});

			}


        /* ------------------------------------------------
				End of magnificPopup
		------------------------------------------------ */

        /* ------------------------------------------------
				modal
		------------------------------------------------ */
		
			$('.modal').modal();
		       
        /* ------------------------------------------------
				End of modal
		------------------------------------------------ */
		 
        /* ------------------------------------------------
				datepicker
		------------------------------------------------ */
			
			if($('.datepicker').length){

				$('.datepicker').datepicker({
					autoClose: true,
					i18n:{
						cancel: 'Закрыть',
      					clear: 'Очистить',
						months:['Январь','Февраль','Март','Апрель','Май','Июнь','Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],
						monthsShort:['Янв','Фев','Мар','Апр','Май','Июн','Июл','Авг','Сен','Окт','Ноя','Дек'],
						weekdays:['Воскресение','Понедельник','Вторник','Среда','Четверг','Пятница','Суббота'],
						weekdaysShort:['Вс','Пн','Вт','Ср','Чт','Пт','Сб'],
						weekdaysAbbrev:['В','П','В','С','Ч','П','С']

					}
				});
			
			}
		       
        /* ------------------------------------------------
				End of datepicker
		------------------------------------------------ */
		 

        /* ------------------------------------------------
				tooltip
		------------------------------------------------ */
			
			if($('.tooltipped').length){

				$('.tooltipped').tooltip();
			
			}
		       
        /* ------------------------------------------------
				End of tooltip
		------------------------------------------------ */
		 
		/* ------------------------------------------------
				Validate START
		------------------------------------------------ */
			$.validator.addMethod("valueNotEquals", function(value, element, arg){
			  	return arg !== value;
			}, "Value must not equal arg.");

			if($('#form1').length){

				$('#form1').validate({
					
					rules:{

						specification_name: {
							required: true,
							minlength: 2
						},

						to_contract: {
							required: true,
							minlength: 2
						},

						select2: {
							required: true
						},

						select1: {
							required: true 
						}

					},

					messages:{
						select1: { 
							required: "Поле обязательно для заполнения"
						},
						select2: { 
							required: "Поле обязательно для заполнения"
						},

						specification_name: {
							required: "Поле обязательно для заполнения",
							minlength: 'Введите не менее 2 символов.'
						},

						to_contract: {
							required: "Поле обязательно для заполнения",
							minlength: 'Введите не менее 2 символов.'
						},


					}

				});	
				
			}

			if($('#form2').length){

				$('#form2').validate({

					rules:{

						fio: {
							required: true,
							minlength: 2
						},

						organization: {
							required: true,
							minlength: 2
						},

						email: {
							required: true,
							email: true
						},

						requisites: {
							required: true,
							minlength: 5
						},

						tel: {
							required: true
						},

					},

					messages:{

						fio: {
							required: "Поле обязательно для заполнения",
							minlength: 'Введите не менее 2 символов.'
						},

						organization: {
							required: "Поле обязательно для заполнения",
							minlength: 'Введите не менее 2 символов.'
						},

						email: {
							required: "Поле обязательно для заполнения",
							email: "Неправильно набран адрес"
						},

						requisites: {
							required: "Поле обязательно для заполнения",
							minlength: 'Введите не менее 5 символов.'
						},

						tel: {
							required: "Поле обязательно для заполнения"
						}

					}

				});	
				
			}


		/* ------------------------------------------------
				Validate END
		------------------------------------------------ */


	});

	$(window).on('load',function(){

		/* ------------------------------------------------
				Name pudin
		------------------------------------------------ */


        /* ------------------------------------------------
				End of Name pudin
		------------------------------------------------ */

	});

})(jQuery);